declare @INICIO aS DATE = GETDATE()-2;
declare @FIN  DATE = GETDATE()-2;


select DISTINCT
-- referencia
sobi.REFERENCIA
--idv
, sobi.IDV
--taller
,sobi.TALLER
--nombre_taller
, sobi.NOM_TALLER
--localidad 
, cta.LOCALIDAD
-- fecha_apertura
,sobi.FECHA_APERTURA
-- fecha_cierre
,sobi.FECHA_CIERRE

-- serial
, veh.BASTIDOR as SERIAL
-- patente
, veh.MATRICULA as PATENTE
-- recepcionista
, sobi.RECEPCIONISTA
-- kms
, veh.KM as KMS
-- familia
, veh.DES_FAMILIA 
-- modelo
, veh.MODELO
-- des_modelo
, veh.DES_MODELO
-- marca
, veh.MARCA
-- anio_vehiculo
, veh.ANIO_VEHI
-- motor FALTA############
, NULL As MOTOR
-- fec_matricula
, veh.FEC_MATRIC
-- fec_venta
, veh.FEC_PREVISTA_ENTREGA  as FEC_VENTA
-- cta_titular
, veh.CTA_TITULAR
-- categoria
, cta.CATEGORIA
-- des_categoria
, cta.DES_CATEGORIA
--	nombre_completo
, cta.NOMBRE_COMERCIAL
--	rut
, cta.DNI as RUT 
--	nombre
, cta.NOMBRE
--	apellido_1
, cta.APELLIDO_1
--	email
, cta.EMAIL
--	telefono
, cta.TELEFONO
--	tel_movil
, cta.TEL_MOVIL
--	tpo_total_facturado
, SUM(sobi.TPO_TOTAL_FACTURADO) as TPO_TOTAL_FACTURADO
--	imp_total_mo
, MAX(sobi.IMP_TOTAL_MO) as IMP_TOTAL_MO
--	total_facturado
, MAX(sobi.IMP_TOTAL_MO + sobi.IMP_TOTAL_REC + sobi.IMP_TOTAL_IVA + sobi.IMP_TOTAL_LINEA) as TOTAL_FACTURADO
--	des_averia  
, sobi.DES_AVERIA
--, sobi.TIPO_FACTURACION
--, sobi.DES_TIPO_FACTURACION
--, sobi.*

 from QuiterQBI..FTSOBI_PR sobi
 LEFT JOIN QuiterQBI..FMVEHBI_PR veh on sobi.IDV = veh.IDV
LEFT JOIN QuiterQBI..FMCUBI_PR cta on veh.CTA_TITULAR = cta.CUENTA
WHERE
-- Si es Lunes Selecciona desde 3 d�as Atras, Sino s�lo el d�a anterior
	--sobi.FECHA_CIERRE >= @INICIO
	--AND FECHA_CIERRE <= @FIN AND
CAST(sobi.TALLER as INT)  BETWEEN 200 and 299
	AND veh.marca = 'NI'
	AND cta.CATEGORIA  IN ( '1', '2', 'E' ,'10') 
	AND sobi.TIPO_FACTURACION IN ( '1','3')
	AND SOBI.TIPO_OR IN ('1','2','10','3','6','9')
	

GROUP BY sobi.REFERENCIA
,sobi.TALLER
,sobi.FECHA_CIERRE
,sobi.FECHA_APERTURA
, veh.MODELO
, veh.MATRICULA
, veh.MARCA
, veh.KM
, veh.DES_MODELO
, veh.DES_FAMILIA 
, veh.BASTIDOR
, veh.ANIO_VEHI
, sobi.RECEPCIONISTA
, sobi.NOM_TALLER
, sobi.IDV
, cta.LOCALIDAD
, veh.FEC_MATRIC
, veh.FEC_PREVISTA_ENTREGA
, veh.CTA_TITULAR
, cta.CATEGORIA
, cta.DES_CATEGORIA
, cta.NOMBRE_COMERCIAL
, cta.DNI
, cta.NOMBRE
, cta.APELLIDO_1
, cta.EMAIL
, cta.TELEFONO
, cta.TEL_MOVIL
, sobi.DES_AVERIA